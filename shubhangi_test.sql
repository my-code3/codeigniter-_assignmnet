-- phpMyAdmin SQL Dump
-- version 5.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Mar 30, 2021 at 03:23 PM
-- Server version: 10.4.11-MariaDB
-- PHP Version: 7.2.28

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `shubhangi_test`
--

-- --------------------------------------------------------

--
-- Table structure for table `department`
--

CREATE TABLE `department` (
  `id` int(11) NOT NULL,
  `dept_name` varchar(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `department`
--

INSERT INTO `department` (`id`, `dept_name`) VALUES
(1, 'HR'),
(2, 'IT');

-- --------------------------------------------------------

--
-- Table structure for table `login`
--

CREATE TABLE `login` (
  `id` int(11) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `f_name` varchar(255) NOT NULL,
  `l_name` varchar(255) NOT NULL,
  `dept_name` varchar(255) NOT NULL,
  `subdept_name` varchar(255) NOT NULL,
  `file_name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `login`
--

INSERT INTO `login` (`id`, `email`, `password`, `f_name`, `l_name`, `dept_name`, `subdept_name`, `file_name`) VALUES
(1, 'test@gmail.com', 'e10adc3949ba59abbe56e057f20f883e', 'shubhangi', 'pawar', 'IT', 'Web Development', '1617093596download.png'),
(2, 'admin1@gmail.com', 'e10adc3949ba59abbe56e057f20f883e', 'Shubham', 'Rajput', 'HR', 'Payroll', '1617104291download (2).jpg'),
(3, 'admin@gmail.com', 'e10adc3949ba59abbe56e057f20f883e', 'Demo', 'Admin', 'HR', 'Admin', '1617107924download.jpg'),
(4, 'test_admin@gmail.com', 'e10adc3949ba59abbe56e057f20f883e', 'test', 'admin', 'IT', 'Mobile Development', '1617108052download.jpg'),
(70, 'test_user@gmail.com', 'e10adc3949ba59abbe56e057f20f883e', 'Test', 'User', 'IT', 'Mobile Development', '1617109706download.jpg'),
(71, 'user_test@gmail.com', 'e10adc3949ba59abbe56e057f20f883e', 'User', 'Test', 'IT', 'Mobile Development', '1617109849download.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `sub_dept`
--

CREATE TABLE `sub_dept` (
  `id` int(11) NOT NULL,
  `dept_id` int(11) NOT NULL,
  `subdept_name` varchar(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sub_dept`
--

INSERT INTO `sub_dept` (`id`, `dept_id`, `subdept_name`) VALUES
(1, 1, 'Admin'),
(2, 1, 'Payroll'),
(3, 1, 'Performance Management'),
(4, 1, 'Recruitement'),
(5, 2, 'Web Development'),
(6, 2, 'Mobile Development'),
(7, 2, 'Tech Support'),
(8, 2, 'Hardware Support');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `department`
--
ALTER TABLE `department`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `login`
--
ALTER TABLE `login`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `email` (`email`);

--
-- Indexes for table `sub_dept`
--
ALTER TABLE `sub_dept`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `department`
--
ALTER TABLE `department`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `login`
--
ALTER TABLE `login`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=72;

--
-- AUTO_INCREMENT for table `sub_dept`
--
ALTER TABLE `sub_dept`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
